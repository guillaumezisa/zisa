<!DOCTYPE html>
<html>
  <head>
    <script src='../view/style/bootstrap1.js' ></script>
    <script src='../view/style/bootstrap2.js' ></script>
    <script src='../view/style/bootstrap3.js' ></script>
    <link rel='stylesheet' href='../view/style/bootstrap.css' >
    <link rel='stylesheet' href='../view/style/home.css'>
    <link rel='stylesheet' href='../view/style/rebirth.css'>
    <link rel='icon' type='image/png' href='../view/images/z.png'>
    <title>Rebirth</title>
    <meta charset='UTF-8'>

  </head>
  <body>
    <header>
      <nav class='navbar navbar-expand-md navbar-dark fixed-top bg-success'>
        <a class='navbar-brand' href='../controller/home_rebirth.php'>Rebirth </a>
        <div class='collapse navbar-collapse' id='navbarCollapse'>
          <ul class='navbar-nav mr-auto'>
            <li class='nav-item dropdown ml-4 mt-3'>
              <a class='nav-link dropdown-toggle text-dark' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Gestion</a>
              <div class='dropdown-menu active' aria-labelledby='navbarDropdown'>
                <a class='dropdown-item ' href='../controller/home_rebirth.php?voir=classement'>Classement</a>
                <a class='dropdown-item' href='../controller/home_rebirth.php?voir=monde'>Carte du monde</a>
                <a class='dropdown-item' href='../controller/home_rebirth.php?voir=messagerie'>Messagerie</a>
                <div class='dropdown-divider'></div>
                <a class='dropdown-item' href='../controller/home_rebirth.php?voir=option'>Option du compte</a>
                <div class='dropdown-divider'></div>
                <a class='dropdown-item' href='../controller/home_cours.php?voir=cours'>Retour au menu</a>
                <a class='dropdown-item' href='../controller/deconnexion.php'>Déconnexion</a>
              </div>
            </li>
            <li class='nav-item ml-4 '>
            </li>
            <?php
              require("../controller/bdd_connexion.php");
              require("../controller/error_display.php");
              $select_partie ="SELECT * FROM rebirth_parties WHERE id_joueur='".$_SESSION['id']."'";
              $partie = $bdd_connexion->query($select_partie)->fetch();
              $select_habitant = "SELECT * FROM rebirth_habitants WHERE id_partie='".$partie[0]."'";
              $habitant = $bdd_connexion->query($select_habitant)->fetch();
              $nb_hab = $habitant[2]+$habitant[1];

              $select_ressources = "SELECT * FROM rebirth_ressources WHERE id_partie='".$partie[0]."'";
              $ressources = $bdd_connexion->query($select_ressources)->fetch();
              echo "
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img title='Habitants' width='30px' src='../view/images/rebirth/homme.png'>".$nb_hab."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Eau'src='../view/images/rebirth/logo_eau.png'> ".$ressources[1]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Nourriture' src='../view/images/rebirth/nourriture.png'> ".$ressources[7]."</a>
              </li>
              <li class='nav-item ml-4'>

                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Bois' src='../view/images/rebirth/bois.png'> ".$ressources[2]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Pierre' src='../view/images/rebirth/pierre.png'>  ".$ressources[3]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Métal' src='../view/images/rebirth/metal.png'> ".$ressources[4]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Métal haute qualité' src='../view/images/rebirth/metalhq.png'>".$ressources[5]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Souffre'src='../view/images/rebirth/souffre.png'> ".$ressources[6]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'><img width='30px' title='Bonheur' src='../view/images/rebirth/smiley3.png'> ".$ressources[8]."</a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'>Defense </a>
              </li>
              <li class='nav-item ml-4'>
                <a class='nav-link text-dark' href='../controller/home_rebirth.php'>Armement </a>
              </li>
              ";
            ?>
        </div>
      </nav>
    </header><br>
