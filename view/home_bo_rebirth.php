<body>
  <a class="btn btn-dark text-light" href="../controller/home_bo.php" role="button">Retour</a><br><br>
  <div class="container bg-secondary">
    <h1>Gestion des mondes de jeux :</h1><br>
    <div class="btn-group btn-group-justified">
      <a href="../controller/home_bo.php?go=jeux&creer=monde" class="btn btn-dark text-light">Créer un monde</a>
    </div>
    <?php
      if (isset($_GET['success']) && $_GET['success'] === "creer_monde"){
        echo "<font color='green'>Votre monde a bien été ajouter</font>";
      }
    ?>
    <br><br><table class="table table-dark">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Nom</th>
          <th scope="col">Taille</th>
          <th scope="col">Aperçut</th>
          <th scope="col">Modifier</th>
          <th scope="col">Supprimer</th>
          <th scope="col">Message</th>
        </tr>
      </thead>
      <tbody>
        <?php
          include('../controller.home_bo_afficher_mondes.php');
        ?>
      </tbody>
    </table><br>
  </div>
</body>
</html>
