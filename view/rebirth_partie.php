<main role="main">
  <div class="jumbotron bg-light">
    <div class="container">
      <?php
        $rand = rand(1,3);
        $select_partie = "SELECT * FROM rebirth_parties WHERE id_joueur='".$_SESSION['id']."'";
        $partie = $bdd_connexion->query($select_partie)->fetch();
        $game = explode(":",$partie[4]);
        $nb = sqrt(count($game));
        $onb = intval($nb-1);
        echo "<div class='container'>
                <div class='row'>
                  <div class='col-sm'>";
        for ($i=0; $i < count($game); $i++) {
          if ( $game[$i] === "0" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=0&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/desecher.png'></a>";
          }elseif ( $game[$i] === "1" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=1&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/herbe.png'></a>";
          }elseif ( $game[$i] === "2" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=2&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/arbre.png'></a>";
          }elseif ( $game[$i] === "3" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=3&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/minerai.png'></a>";
          }elseif ( $game[$i] === "4"){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=4&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/roche.png'></a>";
          }elseif ( $game[$i] === "5" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=5&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/eau.png'></a>";
          }elseif ( $game[$i] === "6" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=6&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/camps.png'></a>";
          }elseif ( $game[$i] === "7" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=7&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/scierie.png'></a>";
          }elseif ( $game[$i] === "8" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=8&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/mine_minerai.png'></a>";
          }elseif ( $game[$i] === "9" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=9&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/mine_pierre.png'></a>";
          }elseif ( $game[$i] === "10" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=10&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/champs.png'></a>";
          }elseif ( $game[$i] === "11" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=11&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/port.png'></a>";
          }elseif ( $game[$i] === "12" ){
            echo "<a href ='../controller/home_rebirth.php?aller=partie&action=12&location=".$i."&type=".$game[$i]."'><img width='100px' src='../view/images/rebirth/village.png'></a>";
          }
          if ( $i % $nb-$onb === 0 ){
            echo "<br>";
          }
        }
        echo "</div><div class='col-sm'><div id='scrollbox'>";
        if ( isset($_GET['action'])){
          if ($_GET['action'] ==='0' ){
            // ZONE DESECHÉ -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
            echo "<table class='table'>
                    <thead>
                      <tr class='bg-info'>
                        <th scope='col'>Zone Desséché</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>
                          <center><img height='145px' width='250px' src='../view/images/rebirth/dessecher.jpg'></center>
                        </th>
                      </tr>
                      <tr>
                        <td>Ce secteur est complètement déssecher ! <br>
                        Il faudrait envoyer quelques hommes ici pour le décontaminer , <br>
                        mais est ce le bon momment ?
                        </td>
                      </tr>
                      <tr class='bg-success'>
                        <th>Décontamination</th>
                      </tr>
                      <tr>
                        <td>
                          Si vous choisissez de décontaminer le secteur ,<br>
                          vous gagnerez 5% de bonheur et la zone sera constructible.
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la decontamination est de 24 heure pour un habitant .
                          ( Vous pouvez en assigner plusieur pour diminuer le temps )
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Eau : 1000<br>
                          Bois : 250
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des hommes pour decontaminé le secteur .</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='decontamination'>
                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                    </tbody>
                  </table>";
          } elseif($_GET['action'] === '1'){
            if ( $rand === 1 ){
                $photo = "<center><img height='145px' width='250px' src='../view/images/rebirth/photo_herbe1.jpg'></center>";
            } elseif ( $rand === 2 ){
                $photo = "<center><img height='145px' width='250px' src='../view/images/rebirth/photo_herbe2.jpg'></center>";
            } elseif ( $rand === 3 ){
                $photo = "<center><img height='145px' width='250px' src='../view/images/rebirth/photo_herbe3.jpg'></center>";
            }
            // ZONE PROPRE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
            echo "<table class='table'>
                    <thead>
                      <tr class='bg-info'>
                        <th scope='col'>Zone propre</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          ".$photo."
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Ce secteur est propre vous pouvez décider de l'utiliser pour produire de la nourriture ou batir quelque chose ?
                        </td>
                      </tr>
                      <tr class='bg-success'>

                        <th>Construire une ferme</th>
                      </tr>
                      <tr>
                        <th>
                          <center><img width='250px' height='145px' src='../view/images/rebirth/photo_ferme1.jpg'></center>
                        </th>
                      </tr>
                      <tr>
                        <td>
                          Si vous choisissez de consturire une ferme ,<br>
                          vous produirez 15 de nourriture par heure , par agriculteur que vous aurez assigner a cette tache .
                          le maximum d'agriculteur est de 50 .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la construction d'une ferme est de 12 heure pour un habitant .
                          ( Vous pouvez en assigner plusieur pour diminuer le temps )
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Eau : 300<br>
                          Bois : 300<br>
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des habitants construire une ferme .</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='ferme'>
                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                      <tr class='bg-success'>
                        <th>Construire un campement</th>
                      </tr>
                      <tr>
                        <th>
                          <center><img width='250px' height='145px' src='../view/images/rebirth/photo_campement1.jpg'></center>
                        </th>
                      </tr>
                      <tr>
                        <td>
                          Si vous choisissez de consturire un campement ,<br>
                          vous produirez 5% de bonheur dans votre peuple , <br>
                          chaque 24 heure votre peuple produit 1 habitant <br>
                          par couple d'hommes et de femmes .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la construction d'un campement est de 12 heure pour un habitant .
                          ( Vous pouvez en assigner plusieur pour diminuer le temps )
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Bois : 750 <br>
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des habitants construire un campement .</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='campement'>

                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                    </tbody>
                  </table>";
          } elseif($_GET['action'] === '2'){
            echo "<table class='table'>
                    <thead>
                      <tr class='bg-info'>
                        <th scope='col'>Forêt</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>
                          <center><img height='145px' width='250px' src='../view/images/rebirth/photo_foret.jpg'></center>
                        </th>
                      </tr>
                      <tr>
                        <td>Ce secteur est idéal pour batir une scierie <br>ou vous pouvez tous simplement décidé de tous rasé .</h3>

                        </td>
                      </tr>
                      <tr class='bg-success'>
                        <th>Scierie</th>
                      </tr>
                      <tr>
                        <td>
                          Si vous construisez une scierie vous pourrez recolté du bois en assez bonne quantité et vous ne serez jamais a cours ,
                          vous produirez 25 de bois par bucheron et par heure . <br>
                          Le maximum de bucheron assigner a ce travaille est de 100 .<br>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la construction de la scierie <br>
                          est de 2 heure pour un habitant .<br>
                          ( Vous pouvez en assigner plusieurs pour diminuer le temps ).<br>
                          Vous devrez ensuite assigner des bucherons a cette foret .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Bois : 500
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des hommes pour construire la scierie.</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='scierie'>
                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                      <tr class='bg-success'>
                        <th>Déforestation</th>
                      </tr>
                      <tr>
                        <td>
                          Si vous rasez la forêt , vous pourrez alors construire un nouveau batiments
                          et vous beneficierez d'un bon stock de bois ( 10 000 ),<br>
                          mais les arbres ne pousseront plus jamais ici .<br>
                          Détuire ce qu'il reste de notre planète et probablement la pire idée sans vouloir faire culpabilisé .<br>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la deforestation <br>
                          est de 12 heure pour un habitant .<br>
                          ( Vous pouvez en assigner plusieurs pour diminuer le temps ).<br>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Gratuit
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des hommes pour deforester la zone .</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='deforestation'>
                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                    </tbody>
                  </table>";
          } elseif($_GET['action'] === '3'){

                  echo "<table class='table'>
                          <thead>
                            <tr class='bg-info'>
                              <th scope='col'>Terrain riche en mineraux</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>
                                <center><img height='145px' width='250px' src='../view/images/rebirth/photo_mine.jpg'></center>
                              </th>
                            </tr>
                            <tr>
                              <td>
                                Ce secteur est idéal pour batir une mine de minerai .
                              </td>
                            </tr>
                            <tr class='bg-success'>
                              <th>Mine de minerai</th>
                            </tr>
                            <tr>
                              <td>
                                Si vous construisez une mine vous pourrez recolté du metal du souffre et du métal haute qualité en petite quantité ,
                                vous produirez 10 de metal par mineur et par heure , <br>
                                vous produirez 5 de souffre par mineur et par heure ,<br>
                                vous produirez 1 de metal haute qualité par mineur et par heure .<br>
                                Le maximum de mineur assigner a ce travaille est de 100 .
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Le temps nécéssaire a la construction de la mine de minerai <br>
                                est de 12 heure pour un habitant .<br>
                                ( Vous pouvez en assigner plusieurs pour diminuer le temps ).<br>
                                Vous devrez ensuite assigner des mineurs a cette mine .
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Bois : 2000 <br>
                              </td>
                            </tr>
                            <tr>
                              <td>Envoyer des hommes pour construire la mine.</td>
                            </tr>
                            <tr>
                              <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                                <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                                <input type='hidden' name='location' value ='".$_GET['location']."'>
                                <input type='hidden' name='type' value ='".$_GET['type']."'>
                                <input type='hidden' name='action' value ='mine_minerai'>
                                <button class='btn btn-dark'>Envoyer</button>
                              </form></td>
                            </tr>
                          </tbody>
                        </table>";
          } elseif($_GET['action'] === '4'){
            echo "<table class='table'>
                    <thead>
                      <tr class='bg-info'>
                        <th scope='col'>Terrain riche en pierre</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>
                          <center><img height='145px' width='250px' src='../view/images/rebirth/photo_mine.jpg'></center>
                        </th>
                      </tr>
                      <tr>
                        <td>
                          Ce secteur est idéal pour batir une mine de pierre .
                        </td>
                      </tr>
                      <tr class='bg-success'>
                        <th>Mine de pierre</th>
                      </tr>
                      <tr>
                        <td>
                          Si vous construisez une mine vous pourrez recolter de la pierre ,
                          vous produirez 10 de pierre par mineur et par heure .<br>
                          Le maximum de mineur assigner a ce travaille est de 100 .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la construction de la mine de pierre <br>
                          est de 12 heure pour un habitant .<br>
                          ( Vous pouvez en assigner plusieurs pour diminuer le temps ).<br>
                          Vous devrez ensuite assigner des mineurs a cette mine .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Bois : 750 <br>
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des hommes pour construire la mine.</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='mine_pierre'>
                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                    </tbody>
                  </table>";
          } elseif($_GET['action'] === '5'){
            echo "<table class='table'>
                    <thead>
                      <tr class='bg-info'>
                        <th scope='col'>Lac</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>
                          <center><img height='145px' width='250px' src='../view/images/rebirth/photo_lac.jpg'></center>
                        </th>
                      </tr>
                      <tr>
                        <td>
                          Ce secteur est idéal pour batir un port .
                        </td>
                      </tr>
                      <tr class='bg-success'>
                        <th>Port</th>
                      </tr>
                      <tr>
                        <td>
                          Si vous construisez un port vous pourrez pecher du poisson ,
                          vous produirez 10 de nourriture par pecheur et produirait 5% de bonheur lors de la construction0 .<br>
                          Le maximum de pecheur assigner a ce travaille est de 75 .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Le temps nécéssaire a la construction d'un port <br>
                          est de 12 heure pour un habitant .<br>
                          ( Vous pouvez en assigner plusieurs pour diminuer le temps ).<br>
                          Vous devrez ensuite assigner des pecheurs a ce port .
                        </td>
                      </tr>
                      <tr>
                        <td>
                          PRIX
                        </td>
                      </tr>
                      <tr>
                        <td>Envoyer des hommes pour construire le port.</td>
                      </tr>
                      <tr>
                        <td><form action='../controller/rebirth_nouvelle_action.php' method='GET'>
                          <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
                          <input type='hidden' name='location' value ='".$_GET['location']."'>
                          <input type='hidden' name='type' value ='".$_GET['type']."'>
                          <input type='hidden' name='action' value ='port'>
                          <button class='btn btn-dark'>Envoyer</button>
                        </form></td>
                      </tr>
                    </tbody>
                  </table>";
          } elseif($_GET['action'] === '6'){
            echo " <h3>Ce secteur contient actuellement un campement , il permet a vos habitants d'avoir un endroit ou vivre .</h3>
            Améliorer en village .<br>
            Un village permet d'augmenter le bonheur de vos habitants et donc d'augmenter vos chances d'agrandir votre peuple en ajoutant 1000 place a votre peuple.
            Le village se construit en 12 heures pour un habitant .<br>
            Construire un village augmentera le bonheur de votre peuple de 10% .
            <form action='../controller/rebirth_nouvelle_action.php' method='GET'>
              <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
              <input type='hidden' name='location' value ='".$_GET['location']."'>
              <input type='hidden' name='type' value ='".$_GET['type']."'>
              <input type='hidden' name='action' value ='village'>
              <button class='btn btn-dark'>Envoyer</button>
            </form><br>

            Détruire le campement .<br>
            Vous récupèrerez 30% des ressources pour la construction de celui ci et perderez 5% de bonheur .<br>
            <form action='../controller/rebirth_nouvelle_action.php' method='GET'>
              <input type='text' size='35px' name='nb' value='' placeholder='Nombre d habitants a envoyer'>
              <input type='hidden' name='location' value ='".$_GET['location']."'>
              <input type='hidden' name='type' value ='".$_GET['type']."'>
              <input type='hidden' name='action' value ='detruire'>
              <button class='btn btn-dark'>Envoyer</button>
            </form><br><br>
            ";
          }
        } else {
          echo "Cliquer sur un secteur pour choisir l'activité de celui ci .<br>";
          if (isset($_GET['erreur'])){
            if ($_GET['erreur'] === "information"){
              echo "Une erreur a eut lieu du a un manque d'information .";
            }elseif($_GET['erreur'] === "construction"){
              echo "Une construction est déja en cours dans ce secteur .";
            }elseif($_GET['erreur'] === "ressources"){
              echo "Ressources inssufisante .";
            }
          } elseif(isset($_GET['success'])){
            echo "Les habitants ce mettent au travaille .";
          }
        }
        echo "</div></div></div></dvi>";
      ?>
    </div>
  </div>
</main>
