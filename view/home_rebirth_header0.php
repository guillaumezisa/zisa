<!DOCTYPE html>
<html>
  <head>
    <script src='../view/style/bootstrap1.js' ></script>
    <script src='../view/style/bootstrap2.js' ></script>
    <script src='../view/style/bootstrap3.js' ></script>
    <link rel='stylesheet' href='../view/style/bootstrap.css' >
    <link rel='stylesheet' href='../view/style/home.css'>
    <link rel='icon' type='image/png' href='../view/images/z.png'>
    <title>Rebirth</title>
    <meta charset='UTF-8'>

  </head>
  <body>
    <header>
      <nav class='navbar navbar-expand-md navbar-dark fixed-top bg-success'>
        <a class='navbar-brand' href='../controller/home_rebirth.php'>Rebirth </a>
        <div class='collapse navbar-collapse' id='navbarCollapse'>
          <ul class='navbar-nav mr-auto'>
            <li class='nav-item dropdown ml-4'>
              <a class='nav-link dropdown-toggle text-dark' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Gestion</a>
              <div class='dropdown-menu active' aria-labelledby='navbarDropdown'>
                <a class='dropdown-item ' href='../controller/home_rebirth.php?voir=classement'>Classement</a>
                <a class='dropdown-item' href='../controller/home_rebirth.php?voir=monde'>Carte du monde</a>
                <a class='dropdown-item' href='../controller/home_rebirth.php?voir=messagerie'>Messagerie</a>
                <div class='dropdown-divider'></div>
                <a class='dropdown-item' href='../controller/home_rebirth.php?voir=option'>Option du compte</a>
                <div class='dropdown-divider'></div>
                <a class='dropdown-item' href='../controller/home_cours.php?voir=cours'>Retour au menu</a>
                <a class='dropdown-item' href='../controller/deconnexion.php'>Déconnexion</a>
              </div>
            </li>
            <li class='nav-item ml-4 '>
            </li>
        </div>
      </nav>
    </header><br>
