<?php
  require("../controller/bdd_connexion.php");
  require("../controller/loged_or_not.php");
  require("../controller/error_display.php");
  require('../controller/afk.js');

  if ( isset($_GET['nb']) && isset($_GET['location']) && isset($_GET['type']) && isset($_GET['action'])){
    //ZONE ARRIDE OK
    if ($_GET['type'] === '0'){
      require("../model/rebirth_actions.php");
      $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
      if ($count_construction[0] != 0 ){
        header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
      }else {
        $construction = $bdd_connexion->query($select_construction)->fetch();
        $partie = $bdd_connexion->query($select_partie)->fetch();
        require("../model/rebirth_actions.php");
        $habitants = $bdd_connexion->query($select_habitants)->fetch();
        $nb_habitants = $habitants[1]+$habitants[2];
        $ressources = $bdd_connexion->query($select_ressource)->fetch();
        $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
        if ($_GET['action'] === "decontamination"){
          $eau_joueur = $ressources[1];
          $bois_joueur = $ressources[2];
          $eau_construction = $construction[3];
          $bois_construction = $construction[4];
          $eau_joueur_fin = $eau_joueur-$eau_construction;
          $bois_joueur_fin = $bois_joueur-$bois_construction;
          if ( $eau_joueur >= $eau_construction && $bois_joueur >= $bois_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_decontamination)->execute();
            $update_eau = $bdd_connexion->prepare($update_ressource_eau)->execute();
            $update_bois = $bdd_connexion->prepare($update_ressource_bois)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        }else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
        }
      }
    //ZONE DE PLAINE OK
    }elseif ($_GET['type'] === '1'){
      require("../model/rebirth_actions.php");
      $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
      if ($count_construction[0] != 0 ){
        header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
      }else {
        $construction = $bdd_connexion->query($select_construction)->fetch();
        $partie = $bdd_connexion->query($select_partie)->fetch();
        require("../model/rebirth_actions.php");
        $habitants = $bdd_connexion->query($select_habitants)->fetch();
        $nb_habitants = $habitants[1]+$habitants[2];
        $ressources = $bdd_connexion->query($select_ressource)->fetch();
        $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
        if ($_GET['action'] === "campement"){
          $bois_joueur = $ressources[2];
          $bois_construction = $construction[4];
          $bois_joueur_fin = $bois_joueur-$bois_construction;
          $bonheur_joueur_fin = $ressources[8]+5;
          if ($bois_joueur >= $bois_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_campement)->execute();
            $update_bois = $bdd_connexion->prepare($update_ressource_bois)->execute();
            $update_bonheur = $bdd_connexion->prepare($update_ressource_bonheur)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        } elseif($_GET['action'] === "ferme"){
          $eau_joueur = $ressources[1];
          $bois_joueur = $ressources[2];
          $eau_construction = $construction[3];
          $bois_construction = $construction[4];
          $eau_joueur_fin = $eau_joueur-$eau_construction;
          $bois_joueur_fin = $bois_joueur-$bois_construction;
          if ( $eau_joueur >= $eau_construction && $bois_joueur >= $bois_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_ferme)->execute();
            $update_eau = $bdd_connexion->prepare($update_ressource_eau)->execute();
            $update_bois = $bdd_connexion->prepare($update_ressource_bois)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        }
      }

    //ZONE DE FORET
    }elseif ($_GET['type'] === '2'){
      require("../model/rebirth_actions.php");
      $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
      if ($count_construction[0] != 0 ){
        header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
      }else {
        require("../model/rebirth_actions.php");
        $construction = $bdd_connexion->query($select_construction)->fetch();
        $partie = $bdd_connexion->query($select_partie)->fetch();
        require("../model/rebirth_actions.php");
        $habitants = $bdd_connexion->query($select_habitants)->fetch();
        $nb_habitants = $habitants[1]+$habitants[2];
        $ressources = $bdd_connexion->query($select_ressource)->fetch();
        $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
        if ($_GET['action'] === "scierie"){
          $bois_joueur = $ressources[2];
          $bois_construction = $construction[4];
          $bois_joueur_fin = $bois_joueur-$bois_construction;
          $bonheur_joueur_fin = $ressources[8]-5;
          if ( $bois_joueur >= $bois_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_scierie)->execute();
            $update_bois = $bdd_connexion->prepare($update_ressource_bois)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        } elseif($_GET['action'] === "deforestation"){
          $bonheur_joueur_fin = $ressources[8]-5;
          if ( $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_deforestation)->execute();
            $update_bonheur = $bdd_connexion->prepare($update_ressource_bonheur)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        }
      }
    //ZONE DE MINERAI
    }elseif ($_GET['type'] === '3'){
      require("../model/rebirth_actions.php");
      $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
      if ($count_construction[0] != 0 ){
        header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
      }else {
        require("../model/rebirth_actions.php");
        $construction = $bdd_connexion->query($select_construction)->fetch();
        $partie = $bdd_connexion->query($select_partie)->fetch();
        require("../model/rebirth_actions.php");
        $habitants = $bdd_connexion->query($select_habitants)->fetch();
        $nb_habitants = $habitants[1]+$habitants[2];
        $ressources = $bdd_connexion->query($select_ressource)->fetch();
        $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
        if ($_GET['action'] === "mine_minerai"){
          $bois_joueur = $ressources[2];
          $bois_construction = $construction[4];
          $bois_joueur_fin = $bois_joueur-$bois_construction;
          require("../controller/rebirth_ressources.php");
          if ( $bois_joueur >= $bois_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_mine_minerai)->execute();
            $update_bois = $bdd_connexion->prepare($update_ressource_bois)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        }
      }
    //ZONE DE PIERRE
    }elseif ($_GET['type'] === '4'){
      require("../model/rebirth_actions.php");
      $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
      if ($count_construction[0] != 0 ){
        header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
      }else {
        require("../model/rebirth_actions.php");
        $construction = $bdd_connexion->query($select_construction)->fetch();
        $partie = $bdd_connexion->query($select_partie)->fetch();
        require("../model/rebirth_actions.php");
        $habitants = $bdd_connexion->query($select_habitants)->fetch();
        $nb_habitants = $habitants[1]+$habitants[2];
        $ressources = $bdd_connexion->query($select_ressource)->fetch();
        $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
        if ($_GET['action'] === "mine_pierre"){
          require("../controller/rebirth_ressources.php");
          $bois_joueur = $ressources[2];
          $bois_construction = $construction[4];
          $bois_joueur_fin = $bois_joueur-$bois_construction;
          if ( $bois_joueur >= $bois_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_mine_pierre)->execute();
            $update_bois = $bdd_connexion->prepare($update_ressource_bois)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        }
      }
    //ZONE EAU
    }elseif ($_GET['type'] === '5'){
      require("../model/rebirth_actions.php");
      $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
      if ($count_construction[0] != 0 ){
        header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
      }else {
        require("../model/rebirth_actions.php");
        $construction = $bdd_connexion->query($select_construction)->fetch();
        $partie = $bdd_connexion->query($select_partie)->fetch();
        require("../model/rebirth_actions.php");
        $habitants = $bdd_connexion->query($select_habitants)->fetch();
        $nb_habitants = $habitants[1]+$habitants[2];
        $ressources = $bdd_connexion->query($select_ressource)->fetch();
        $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
        if ($_GET['action'] === "port"){
          require("../controller/rebirth_ressources.php");
          if ( $eau_joueur >= $eau_construction && $bois_joueur >= $bois_construction && $pierre_joueur >= $pierre_construction && $metal_joueur >= $metal_construction && $metalhq_joueur >= $metalhq_construction && $souffre_joueur >= $souffre_construction && $nourriture_joueur >= $nourriture_construction && $_GET['nb'] <= $nb_habitants){
            $temps = $construction[2]*60*60 / $_GET['nb'];
            $my_date=date("Y-m-d H:i:s");
            $my_date_time=time("Y-m-d H:i:s");
            $my_new_date_time=$my_date_time+$temps;
            $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
            require("../model/rebirth_actions.php");
            $construction = $bdd_connexion->prepare($insert_construction_port)->execute();
            header("location:../controller/home_rebirth.php?aller=partie&success=action");
          } else {
            header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
          }
        }
      }
    //
  }elseif ($_GET['type'] === '6'){
    require("../model/rebirth_actions.php");
    $count_construction = $bdd_connexion->query($select_count_construction)->fetch();
    if ($count_construction[0] != 0 ){
      header("location:../controller/home_rebirth.php?aller=partie&erreur=construction");
    }else {
      $construction = $bdd_connexion->query($select_construction)->fetch();
      $partie = $bdd_connexion->query($select_partie)->fetch();
      require("../model/rebirth_actions.php");
      $habitants = $bdd_connexion->query($select_habitants)->fetch();
      $nb_habitants = $habitants[1]+$habitants[2];
      $ressources = $bdd_connexion->query($select_ressource)->fetch();
      $action_construction = $bdd_connexion->query($select_action_construction)->fetch();
      if ($_GET['action'] === "village"){
        require("../controller/rebirth_ressources.php");
        if ( $eau_joueur >= $eau_construction && $bois_joueur >= $bois_construction && $pierre_joueur >= $pierre_construction && $metal_joueur >= $metal_construction && $metalhq_joueur >= $metalhq_construction && $souffre_joueur >= $souffre_construction && $nourriture_joueur >= $nourriture_construction && $_GET['nb'] <= $nb_habitants){
          $temps = $construction[2]*60*60 / $_GET['nb'];
          $my_date=date("Y-m-d H:i:s");
          $my_date_time=time("Y-m-d H:i:s");
          $my_new_date_time=$my_date_time+$temps;
          $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
          require("../model/rebirth_actions.php");
          $construction = $bdd_connexion->prepare($insert_construction_village)->execute();
          header("location:../controller/home_rebirth.php?aller=partie&success=action");
        } else {
          header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
        }
      } elseif($_GET['action'] === "detruire"){
        require("../controller/rebirth_ressources.php");
          if ( $eau_joueur >= $eau_construction && $bois_joueur >= $bois_construction && $pierre_joueur >= $pierre_construction && $metal_joueur >= $metal_construction && $metalhq_joueur >= $metalhq_construction && $souffre_joueur >= $souffre_construction && $nourriture_joueur >= $nourriture_construction && $_GET['nb'] <= $nb_habitants){
          $temps = $construction[2]*60*60 / $_GET['nb'];
          $my_date=date("Y-m-d H:i:s");
          $my_date_time=time("Y-m-d H:i:s");
          $my_new_date_time=$my_date_time+$temps;
          $my_new_date=date("Y-m-d H:i:s",$my_new_date_time);
          require("../model/rebirth_actions.php");
          $construction = $bdd_connexion->prepare($insert_construction_detruire)->execute();
          header("location:../controller/home_rebirth.php?aller=partie&success=action");
        } else {
          header("location:../controller/home_rebirth.php?aller=partie&erreur=ressources");
        }
      }
    }
    ///==///
    } else {
      header("location:../controller/home_rebirth.php?aller=partie&erreur=information");
    }
  }
?>
