<?php
  require("../controller/bdd_connexion.php");
  require("../controller/loged_or_not_admin.php");
  require("../model/bo_rebirth.php");
  if ( !isset($_GET['id'])){
    header('location:../controller/home_bo.php?go=jeux');
  } else {
    $monde1 = $bdd_connexion->query($req_afficher_monde)->fetch();
    echo "<h1>Aperçut de la map complète : ".$monde1[1]."</h1><br>";

    //DÉCOMPACTAGE
    $monde = explode("|",$monde1[0]);
    for ($i=0; $i < count($monde) ; $i++) {
      $case = explode(",",$monde[$i]);
        for ($y=0; $y < count($case) ; $y++) {
          $nouveau_monde[$i][$y] = $case[$y];
        }
      }
    }

    //AFFICHAGE
    for ( $i = 0 ; $i< count($nouveau_monde) ; $i++ ){
      if ($i % 22 === 0 ){
        echo "<br>";
      }
      if ( $nouveau_monde[$i][0] === 	"terre" ){
        echo "<img src='../view/images/rebirth/terre.jpg' width='50px'>";
      } elseif( $nouveau_monde[$i][0] === "plaine" ){
        echo "<img src='../view/images/rebirth/herbe.jpg' width='50px'>";
      } elseif( $nouveau_monde[$i][0] === "roche" ){
        echo "<img src='../view/images/rebirth/roche.jpg' width='50px'>";
      } elseif( $nouveau_monde[$i][0] === "lac" ){
        echo "<img src='../view/images/rebirth/eau.jpg' width='50px'>";
      }
    }
  ?>
