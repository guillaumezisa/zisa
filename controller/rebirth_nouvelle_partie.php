<?php
  require("../controller/bdd_connexion.php");
  require("../controller/loged_or_not.php");
  require("../controller/error_display.php");
  if ( isset($_GET['monde']) ){
    $select_verification_joueur ="SELECT * FROM rebirth_parties WHERE id_joueur='".$_SESSION['id']."'";
    $verification = $bdd_connexion->query($select_verification_joueur)->fetch();
    if ( empty($verification)){
      $select_emplacement_libre = "SELECT * FROM rebirth_mondes WHERE id_monde='".$_GET['monde']."'";
      $emplacement = $bdd_connexion->query($select_emplacement_libre)->fetch();
      $emplacements = explode("|",$emplacement[3]);
      $nb = count($emplacements);
      $insert = false ;
      while( $insert != true ){
        $random = rand(0,$nb);
        if ( "0" === $emplacements[$random]){
          $insert = true ;
        } else {
          $insert = false ;
        }
      }
      $carte[0] = [0,0,0,0,0];
      $carte[1] = [0,0,0,0,0];
      $carte[2] = [0,0,0,0,0];
      $carte[3] = [0,0,0,0,0];
      $carte[4] = [0,0,0,0,0];

      for ($i=0; $i < count($carte); $i++) {
        for ($y=0; $y < count($carte[$i]) ; $y++) {
          $random_zone = rand(0,100);
          if ( $random_zone <= 20 ){
            $carte[$i][$y] = 0;
          }elseif( $random_zone <= 50){
            $carte[$i][$y] = 1;
          }elseif( $random_zone <= 65){
            $carte[$i][$y] = 2;
          }elseif( $random_zone <= 75){
            $carte[$i][$y] = 3;
          }elseif( $random_zone <= 85){
            $carte[$i][$y] = 4;
          }elseif( $random_zone <= 100){
            $carte[$i][$y] = 5;
          }
        }
      }
      $carte[2][2] = 6;

      for ($i=0; $i < count($carte); $i++) {
        for ($y=0; $y < count($carte[$i]); $y++) {
          if ( $i === 0 && $y === 0){
            $ligne = $carte[$i][$y];
          }else{
            $ligne = $ligne.":".$carte[$i][$y];
          }
        }
      }
      echo $ligne;
      $insert_partie = "INSERT INTO `rebirth_parties` (`id_partie`, `id_monde`, `id_joueur`, `niveau_joueur`, `carte_joueur`) VALUES (NULL, '".$_GET['monde']."','".$_SESSION['id']."','1','".$ligne."')";
      $partie = $bdd_connexion->prepare($insert_partie)->execute();

      $select_id_partie = "SELECT * FROM rebirth_parties WHERE id_joueur='".$_SESSION['id']."'";
      $id_partie = $bdd_connexion->query($select_id_partie)->fetch();

      $insert_habitants = "INSERT INTO `rebirth_habitants` (`id_partie`, `habitant_homme`, `habitant_femme`) VALUES ($id_partie[0] , '5', '5');";
      $habitants = $bdd_connexion->prepare($insert_habitants)->execute();

      $insert_ressource ="INSERT INTO `rebirth_ressources` (`id_partie`, `eau`, `bois`, `pierre`, `metal`,`metalhq`,`souffre`, `nourriture`, `bonheur`) VALUES ($id_partie[0], '1000', '1000', '1000','0','0','0', '1000', '50');";
      $ressource = $bdd_connexion->prepare($insert_ressource)->execute();

      $select_partie = "SELECT * FROM rebirth_parties WHERE id_joueur='".$_SESSION['id']."'";
      $partie = $bdd_connexion->query($select_partie)->fetch();
      $game = explode(":",$partie[4]);

      for ($i=0; $i < count($game); $i++) {
        $insert_zone = "INSERT INTO `rebirth_zones` (`id_partie`,`id_zone`,`id_type`, `niveau_construction`, `niveau_defense`) VALUES ( ".$id_partie[0].",".$i.", ".$game[$i].", '1', '1');";
        $zone = $bdd_connexion->prepare($insert_zone)->execute();
      }

      header("location:../controller/home_rebirth.php?aller=partie");
    } else {
      header("location:../controller/home_rebirth.php?aller=partie");
    }
  } else {
    header("location:../controller/home_rebirth.php");
  }
?>
